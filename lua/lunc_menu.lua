LUNCMod             = LUNCMod or {}
LUNCMod._menu_path  = ModPath .. 'menu.txt'
LUNCMod._save_path  = SavePath .. 'led_uncombo.txt'
LUNCMod._loc_path   = ModPath .. 'loc/'
LUNCMod._settings   = {
  disabled_gadget = 1
}
LUNCMod._lookup = {
  "",                                     -- None
  "wpn_fps_upg_fl_ass_utg",               -- Laser
  "wpn_fps_upg_fl_ass_peq15_flashlight"   -- Flashlight
}

-- Load/Save Settings
function LUNCMod.load()
  local file = io.open(LUNCMod._save_path, "r")

  if file then
    local settings = json.decode(file:read())

    for k, v in pairs(settings) do
      LUNCMod._settings[k] = v
    end

    file:close()
  end
end

function LUNCMod.save()
  local file      = io.open(LUNCMod._save_path, "w")

  if file then
    local settings  = json.encode(LUNCMod._settings)

    file:write(settings)
    file:close()
  end
end

-- Localization file
function LUNCMod.get_loc_file()
  local lang = BLT.Localization:get_language().language

  if lang then
    local file = LUNCMod._loc_path .. lang .. '.txt'

    if io.file_is_readable(file) then
      return file
    end
  end

  return LUNCMod._loc_path .. 'en.txt'
end

function LUNCMod.loc_init()
  Hooks:AddHook('LocalizationManagerPostInit', 'lunc_loc_postinit_id',
    function(self)
      local loc_file = LUNCMod.get_loc_file()

      self:load_localization_file(loc_file, false)
    end
  )
end

-- Callbacks
function LUNCMod.menu_callback_init()
  MenuCallbackHandler.lunc_disabled_gadget_multi_clbk = function (self, item)
    local value = item:value()

    LUNCMod._settings.disabled_gadget = value
    LUNCMod.save()
  end
end

-- Init Mod Options
function LUNCMod.menu_init()
  Hooks:Add('MenuManagerInitialize', 'lunc_menumanagerinit_id',
    function(menu_manager)
      MenuHelper:LoadFromJsonFile(LUNCMod._menu_path, LUNCMod, LUNCMod._settings)
    end
  )
end

function LUNCMod.init()
  LUNCMod.load()
  LUNCMod.loc_init()
  LUNCMod.menu_callback_init()
  LUNCMod.menu_init()
end

LUNCMod.init()