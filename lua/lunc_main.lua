LUNCMod = LUNCMod or {}

function LUNCMod.cycle_gadget_posthook()
  Hooks:PostHook(NewRaycastWeaponBase, "toggle_gadget", "lunc_remove_gadget_id",
    function (self, ...)
      local disabled_gadget = LUNCMod._lookup[LUNCMod._settings.disabled_gadget]
      local is_ledcombo     = self._parts and self._parts["wpn_fps_upg_fl_ass_utg"]

      if is_ledcombo and disabled_gadget ~= "" then
        local gadget_on = self._gadget_on or 0
        local gadgets   = self._gadgets

        if gadgets then
          if gadget_on > 0 and gadgets[gadget_on] == disabled_gadget then
              self:toggle_gadget(...)
          end
        end
      end
    end
  )
end

if RequiredScript == "lib/units/weapons/newraycastweaponbase" then
  LUNCMod.cycle_gadget_posthook()
end