v1.0.5
Fixed:
* Issue that would cause options to not show up while in main menu

v1.0.4
Added:
* French translation courtesy of SCP-5666-Akira

v1.0.3
Added:
* Chinese (Simplified) translation courtesy of Arknights

v1.0.2
Fixed:
* issue affecting attachments other than LED Combo

v1.0.1
Fixed:
* issue with saved settings not loading

v1.0
* Release