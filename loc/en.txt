{
  "lunc_menu_title": "LED Uncombo",
  "lunc_menu_desc": "Disable the Laser or Flashlight on the LED Combo attachment",
  "lunc_disabled_gadget_multi_title": "Disabled Gadget",
  "lunc_disabled_gadget_multi_desc": "Select the gadget to disable",
  "lunc_disabled_gadget_multi_item_1": "None",
  "lunc_disabled_gadget_multi_item_2": "Laser",
  "lunc_disabled_gadget_multi_item_3": "Flashlight"
}